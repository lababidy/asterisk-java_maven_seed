/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asterisk.java;

/**
 *
 * @author sroot
 */
import java.util.regex.Pattern;
import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiRequest;
import org.asteriskjava.fastagi.BaseAgiScript;
/* Example incoming call handler
Answer call, speak message */
public class ExampleCallIn extends BaseAgiScript {
  public void service(AgiRequest request, AgiChannel channel) throws AgiException {
    answer();
//    exec("Playback", "tt-monkeys"); 
String str1 = Pattern.quote("sip/96874@10.253.31.13");
//    exec("Playback", "tt-monkeys"); 
      exec("Dial", "sip/96874@10.253.31.13"); 
    hangup();
  }
}
